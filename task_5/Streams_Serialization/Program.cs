﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Xml.Serialization;

namespace Streams_Serialization
{
    class Program
    {
        static void Main(string[] args)
        {
            menu();
        }
        static public void menu()
        {
            int n = 0;

            do
            {
                Console.Clear();
                Console.WriteLine("1. Task_1");
                Console.WriteLine("2. Task_2");
                Console.WriteLine("0. Exit");
                n = EnterNum();

                    switch (n)
                    {
                        case 1: task_1(); break;
                        case 2: task_2(); break;
                        default: break;
                    }
                Console.ReadKey();
            } while (n != 0);

        }
        static public void task_1()
        {

            HtmlContent html = new HtmlContent("index.txt", "result.txt");
            html.Read();
            html.SearchContent();
            html.Write();

        }
        static public void task_2()
        {

            List<Directory> List = CreateList();
            int n = 0;

                do
                {
                    Console.Clear();
                    Console.WriteLine("1. Binary Serialization");
                    Console.WriteLine("2. Json Serialization");
                    Console.WriteLine("3. Xml Serialization");
                    Console.WriteLine("0. Back");
                    n = EnterNum();

                        switch (n)
                        {

                            case 1: BinarySerialization(List); break;
                            case 2: JsonSerialization(List); break;
                            case 3: XMLSerialization(List); break;
                            default:break;
     
                        }
                    Console.ReadKey();

                } while (n != 0);
        }
        static public void BinarySerialization(List<Directory> list)
        {

            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fs = new FileStream("BinarySerialization.txt", FileMode.OpenOrCreate);

            formatter.Serialize(fs, list);
            fs.Close();

            fs = new FileStream("BinarySerialization.txt", FileMode.OpenOrCreate);
            list = (List<Directory>)formatter.Deserialize(fs);

            for (int i = 0; i < list.Count; i++)
                list[i].ToString();

            fs.Close();
        }
        static public void JsonSerialization(List<Directory> list)
        {
            DataContractJsonSerializer jsonFormatter = new DataContractJsonSerializer(typeof(List<Directory>));

            FileStream fs = new FileStream("JsonSerialization.txt", FileMode.OpenOrCreate);
            jsonFormatter.WriteObject(fs, list);
            fs.Close();

            fs = new FileStream("JsonSerialization.txt", FileMode.OpenOrCreate);
            list = (List<Directory>)jsonFormatter.ReadObject(fs);
            fs.Close();

                for(int i = 0; i < list.Count; i++)
                {
                    list[i].ToString(); 
                }

        }
        static public void XMLSerialization(List<Directory> list)
        {
            
            XmlSerializer formatter = new XmlSerializer(typeof(List<Directory>));

            Stream fs = new FileStream("XmlSerialization.txt", FileMode.OpenOrCreate);
            formatter.Serialize(fs, list);
            fs.Close();

            fs = new FileStream("XmlSerialization.txt", FileMode.OpenOrCreate);
            list = (List<Directory>)formatter.Deserialize(fs);
            fs.Close();

            for (int i = 0; i < list.Count; i++)
            {
                list[i].ToString();
            }
        }
        static public int EnterNum()
        {
            bool check;
            int num;

            do
            {
                check = Int32.TryParse(Console.ReadLine(), out num);

                if (!check)

                    Console.Write("......Entered incorrect data......\n......Repeat please......\n");

            } while (!check);

            return num;
        }
        static public List<Directory> CreateList()
        {
            var list = new List<Directory>();

            StreamReader reader = new StreamReader("directory.txt");

            string text = reader.ReadLine();

            do
            {
                string[] textArr = text.Split('&');
                list.Add(new Directory(textArr[0], textArr[1], textArr[2], textArr[3], textArr[4]));
                text = reader.ReadLine();

            } while (text != null);

            reader.Close();

            return list;
        }
    }
    class HtmlContent
    {
        string file;
        string content = "";
        string readFile;
        string writeFile;


        public string Content{

            get { return content; }

        }
        public string File
        {
            get { return file;}
        }
        public HtmlContent(string read, string write)
        {
            readFile = read;
            writeFile = write;
        }
        public void Read()
        {
            StreamReader reader = new StreamReader(readFile);

            file = reader.ReadToEnd();
      
            reader.Close();
        }
        public void SearchContent()
        {
            Regex reg = new Regex(@"\<[^/]+\>\s*\n*?\r*?\s*([^<>]*)\s*\n*?\r*?\s*\<\/.+\>");
            MatchCollection match = reg.Matches(file);

            for (int i = 0; i < match.Count; i++)
                content += match[i].Groups[1].Value;

            Console.WriteLine("Result in file \"result.txt\"");
        }
        public void Write()
        {
            StreamWriter writer = new StreamWriter(writeFile, false);

            writer.Write(Content);

            writer.Close();
        }
    }

    [Serializable]
    public class Directory
    {
        
        public string NameCompany { get; set; }
        public string Owner { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Occupation { get; set; }

        public Directory(){}
        public Directory(string name, string owner, string phone, string address, string occupation)
        {
            NameCompany = name;
            Owner = owner;
            Phone = phone;
            Address = address;
            Occupation = occupation;
        }
        public new void ToString()
        {
            Console.WriteLine("|____________________|____________________|_______________|____________________|____________________|");
            Console.WriteLine("|{0,20}|{1, 20}|{2, 15}|{3, 20}|{4, 20}|", NameCompany, Owner, Phone, Address, Occupation);
        }
    }

}
