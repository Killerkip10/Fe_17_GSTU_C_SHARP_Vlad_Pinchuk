﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
    class Data: IComparable<Data>
    {
        int day;
        int month;
        int year;

        public int Day {
            get
            {
                return day;
            }
            set
            {
                day = value;
            }
        }
        public int Month
        {
            get
            {
                return month;
            }
            set
            {
                month = value;
            }
        }
        public int Year
        {
            get
            {
                return year;
            }
            set
            {
                year = value;
            }
        }

        public Data()
        {
            day = 15;
            month = 10;
            year = 2017;
        }
        public Data(int day, int month, int year)
        {
            if (day <= 0 || month <= 0 || year <= 0)
            {
                this.day = 15;
                this.month = 10;
                this.year = 2017;
            }
            else
            {
                int allDay = day;
    
                while(month > 12)
                    if (month > 12)
                    {
                        year += 1;
                        month -= 12;
                    }

                if ((day > 31 && (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12))
                    || (day > 30 && (month == 4 || month == 6 || month == 9 || month == 11))
                    || (day > 28 && month == 2 && year % 4 != 0)
                    || (day > 29 && month == 2 && year % 4 == 0))
                {

                    while (true)
                    {
                        if (month > 12)
                        {
                            year += 1;
                            month -= 12;
                        }
                        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
                        {
                            if (allDay <= 31)
                                break;

                            allDay -= 31;
                            month++;
                            continue;
                        }
                        if (month == 4 || month == 6 || month == 9 || month == 11)
                        {
                            if (allDay <= 30)
                                break;


                            allDay -= 30;
                            month++;
                            continue;
                        }
                        if (month == 2 && year % 4 == 0)
                        {
                            if (allDay <= 29)
                                break;

                            allDay -= 29;
                            month++;
                            continue;
                        }
                        if (month == 2 && year % 4 != 0)
                        {
                            if (allDay <= 28)
                                break;

                            allDay -= 28;
                            month++;
                            continue;
                        }
                    }
                }
                this.day = allDay;
                this.month = month;
                this.year = year;
            }
        }
        public new string ToString()
        {
            return day + "." + month + "." + year;
        }

        public int CompareTo(Data obj)
        {
            int first = 365 * this.year + 30 * (this.month - 1) + this.day;
            int second = 365 * obj.year + 30 * (obj.month - 1) + obj.day;

            if (first > second)
                return 1;

            if (first < second)
                return -1;

            return 0;
        }
    }
}
