﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu();
        }
        static public void Menu()
        {
            int n = 0;
            TechnicalCertificate[] arrTech = new TechnicalCertificate[0];

                do
                {
                    Console.Clear();
                    Console.WriteLine("{0, 50}", "______________________________________");
                    Console.WriteLine("{0, 51}", "|  1.      Registry new car          | ");
                    Console.WriteLine("{0, 50}", "|------------------------------------|");
                    Console.WriteLine("{0, 50}", "|  2.        Show all list           |");
                    Console.WriteLine("{0, 50}", "|------------------------------------|");
                    Console.WriteLine("{0, 50}", "|  3. Show cars which are more than  |");
                    Console.WriteLine("{0, 50}", "|            specified date          |");
                    Console.WriteLine("{0, 50}", "|------------------------------------|");
                    Console.WriteLine("{0, 50}", "|  4. Show cars with specified mark  |");
                    Console.WriteLine("{0, 50}", "|------------------------------------|");
                    Console.WriteLine("{0, 50}", "|  5. Show cars suitable specified   |");
                    Console.WriteLine("{0, 50}", "|                 range              |");
                    Console.WriteLine("{0, 50}", "|------------------------------------|");
                    Console.WriteLine("{0, 50}", "|  0.             Exit               |");
                    Console.WriteLine("{0, 50}", "|------------------------------------|");

                    n = EnterNum();

                        switch (n) {

                            case 1: { registryCar(ref arrTech); }break;
                            case 2: { showAll(arrTech); }break;
                            case 3: { searchByDate(arrTech); }break;
                            case 4: { searchByName(arrTech); }break;
                            case 5: { searchByRangeYear(arrTech); } break;
                            case 0: { }break;
                        }
            } while (n != 0);


        }     
        static public void registryCar(ref TechnicalCertificate[]  arrTech)
        {
            
             Array.Resize(ref arrTech, TechnicalCertificate.count + 1);

            Console.Clear();
            Console.Write("Register sign : ");
            string regSign = Console.ReadLine();

            Console.Write("Mark : ");
            string mark = Console.ReadLine();

            Console.Write("Year : ");
            int year = EnterNum();

            Console.Write("Weight : ");
            int weight = EnterNum();

            Console.Write("Amount engine : ");
            int amount = EnterNum();

            Console.WriteLine("....Data registry....");
            Console.Write("Day : ");
            int day = EnterNum();

            Console.Write("Month : ");
            int month = EnterNum();

            Console.Write("Year : ");
            int yearData = EnterNum();
            
            arrTech[TechnicalCertificate.count] = new TechnicalCertificate(regSign, mark, year, weight, amount, day, month, yearData);

            Console.WriteLine("...Entry created...");
        }
        static public void showAll(TechnicalCertificate[] arrTech)
        {
                
            for (int i = 0; i < arrTech.Length; i++)
                Console.WriteLine(arrTech[i].ToString());

            if (arrTech.Length == 0)
                Console.WriteLine("...Nothing...");

            Console.ReadKey();
        }
        static public void searchByDate(TechnicalCertificate[] arrTech)
        {
            Console.WriteLine("Enter data (21.12.1999): ");

            Data presentDate;
            string data = Console.ReadLine();
            
                if (EnterData(data))
                {
                    string[] arrData = data.Split('.');

                    presentDate = new Data(Convert.ToInt32(arrData[0]), Convert.ToInt32(arrData[1]), Convert.ToInt32(arrData[2]));

                    for (int i = 0; i < arrTech.Length; i++)
                        if (arrTech[i].Data.CompareTo(presentDate) == 1)
                            Console.WriteLine(arrTech[i].ToString());

                    if (arrTech.Length == 0)
                        Console.WriteLine("...Nothing...");
                }
                else
                    Console.WriteLine("You enter data doesn't correct");

            Console.ReadKey();
        }
        static public void searchByName(TechnicalCertificate[] arrTech)
        {
            Console.WriteLine("Enter name for search : ");
            var name = Console.ReadLine();

                for(int i = 0; i < arrTech.Length; i++)

                    if (arrTech[i].Mark == name)
                        Console.WriteLine(arrTech[i].ToString());

            Console.ReadKey();
        }
        static public void searchByRangeYear(TechnicalCertificate[] arrTech)
        {
            Console.WriteLine("Enter starting year : ");
            int first = EnterNum();

            Console.WriteLine("Enter ending year : ");
            int second = EnterNum();

                if(first < second)
                {
                    int swap = first;
                    first = second;
                    second = swap;
                }
            
                for (int i = 0; i < arrTech.Length; i++)
                    if (arrTech[i].Year >= second && arrTech[i].Year <= first)
                        Console.WriteLine(arrTech[i].ToString());

            Console.ReadKey();
        }

        static public bool EnterData(string data)
        {
            string[] arrData = data.Split('.');
            int num;
            bool t = false;

            if (arrData.Length == 3)
                for (int i = 0; i < arrData.Length; i++)
                {
                    if (Int32.TryParse(arrData[i], out num))
                        t = true;
                    else
                    {
                        t = false;

                        break;
                    }
                }

            return t;
        }
        static public int EnterNum()
        {
            bool check;
            int num;

            do
            {
                check = Int32.TryParse(Console.ReadLine(), out num);

                if (!check)

                    Console.Write("......Entered incorrect data......\n......Repeat please......\n");

            } while (!check);

            return num;
        }
    }
}
