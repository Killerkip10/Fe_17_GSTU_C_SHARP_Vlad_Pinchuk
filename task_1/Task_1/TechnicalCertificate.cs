﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
    class TechnicalCertificate: IComparable<TechnicalCertificate>
    {
        string registreSign;
        string mark;
        int year;
        int weight;
        int amount;
        Data data;

        static public int count = 0;

        public string RegisterSign
        {
            get
            {
                return registreSign;
            }
            set
            {
                registreSign = value;
            }
        }
        public string Mark
        {
            get
            {
                return mark;
            }
            set
            {
                mark = value;
            }
        }
        public int Year
        {
            get
            {
                return year;
            }
            set
            {
                year = value;
            }
        }
        public int Weight
        {
            get
            {
                return weight;
            }
            set
            {
                weight = value;
            }
        }
        public int Amount
        {
            get
            {
                return amount;
            }
            set
            {
                amount = value;
            }
        }
        public Data Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }
   
        public TechnicalCertificate(string reg, string mark, int year, int weight, int amount, int day, int month, int yearData)
        {
            registreSign = reg;
            this.mark = mark;
            this.year = year;
            this.weight = weight;
            this.amount = amount;

            data = new Data(day, month, yearData);
            count++;
        }
        public new string ToString()
        {
            return "Register Sign : " + registreSign + "; Mark : " + mark + "; Year : " + year + "; Weight : " + weight + "; Amount : " + amount + "; Data registry : " + data.ToString();
        }
        static public void counter()
        {
            Console.Write(count);
        }         

        public int CompareTo(TechnicalCertificate obj)
        {
            if (year > obj.year)
                return 1;

            if (year < obj.year)
                return -1;

            return 0;
        }
    }
}
