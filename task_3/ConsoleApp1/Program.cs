﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            menu();

            Console.ReadKey();
        }
        static void menu()
        {
            int n;

            do
            {
                Console.Clear();
                Console.WriteLine("1. Task 1");
                Console.WriteLine("2. Task 2");
                Console.WriteLine("0. Exit");
                n = EnterNum();

                    switch (n)
                    {
                        case 1:

                            task_1();
                            break;
                        case 2:

                            task_2();
                            break;
                        default:
                            break;
                    }

            } while (n != 0);

        }
        static public int EnterNum()
        {
            bool check;
            int num;

            do
            {
                check = Int32.TryParse(Console.ReadLine(), out num);

                if (!check)

                    Console.Write("........Введены неверные данные, попробуйте ещё раз........\n");

            } while (!check);

            return num;
        }
        static public void task_1()
        {
            Console.Clear();

            Console.Write("Enter string : ");
            string text = Console.ReadLine();

            int lower = 0;
            int upper = 0;
            int punct = 0;
            int space = 0;

                for(int i = 0; i < text.Length; i++)
                {
                    if (Char.IsLower(text[i]))
                    {
                        lower++;
                        continue;
                    }
                    if (Char.IsUpper(text[i]))
                    {
                        upper++;
                        continue;
                    }
                    if(text[i] == ' ')
                    {
                        space++;
                        continue;
                    }
                }

            Regex reg = new Regex(@"\W");

            punct = reg.Matches(text).Count - space;

            Console.WriteLine("Lower symbol : " + lower);
            Console.WriteLine("Upper symbol : " + upper);
            Console.WriteLine("Space : " + space);
            Console.WriteLine("Punctuation : " + punct);
            Console.ReadKey();
        }
        static public void task_2()
        {
            Console.Clear();
            Console.WriteLine("Calculator");
            Console.WriteLine("Enter string : ");

            string text = Console.ReadLine();

            if (check(text))
            {
                Regex reg = new Regex(@"\([\d+\s*\+\-\*\/]*\){1}");

                while (reg.IsMatch(text))
                {
                    Match items = reg.Match(text);
                    string res = checkMultDiv(items);
                    text = text.Replace(Convert.ToString(items), res);
                    items = reg.Match(text);

                    text = swap(text);
                    items = reg.Match(text);

                    res = checkSumSubstr(Convert.ToString(items));
                    text = text.Replace(Convert.ToString(items), res);

                    text = swap(text);
                }

                Regex last = new Regex(".*");

                text = checkMultDiv(last.Match(text));
                text = swap(text);
                text = checkSumSubstr(text);
                text = swap(text);

                Console.WriteLine("Your answer : " + text);
            }
            else
                Console.WriteLine("Your entered uncorrect exaple");

            Console.ReadKey();
        }
        static public bool check(string line)
        {
            int l = 0;
            int r = 0;

                for(int i = 0; i < line.Length; i++)
                {

                    if (line[i] == '(')
                        l++;

                    if (line[i] == ')')
                        r++;
                }
                if (l != r)
                    return false;
            
            Regex reg = new Regex(@"[0-9\+\-\*\/\(\)\s]");

                if (reg.Matches(line).Count != line.Length)
                    return false;

            return true;
        }
        static public string checkMultDiv(Match text)
        {
            string text2 = Convert.ToString(text);
            Regex multDiv = new Regex(@"\s*\-?\d+\s*[\/\*]\s*\-?\d+\s*");
            Regex multDiv2 = new Regex(@"\s*\-?\d+\s*");
            Regex multDiv3 = new Regex(@"[\/\*]");

            Console.WriteLine();

                while (multDiv.IsMatch(text2))
                {
                    string value = Convert.ToString(multDiv.Match(text2));
                    string valueNew = "";    

                    MatchCollection ab = multDiv2.Matches(value);
                    Match type = multDiv3.Match(value);


                        if(Convert.ToString(type) == "/")
                        {

                            valueNew = Convert.ToString(Convert.ToInt32(Convert.ToString(ab[0])) / Convert.ToInt32(Convert.ToString(ab[1])));
                        }
                        if (Convert.ToString(type) == "*")
                        {

                            valueNew = Convert.ToString(Convert.ToInt32(Convert.ToString(ab[0])) * Convert.ToInt32(Convert.ToString(ab[1])));
                        }

                    text2 = text2.Replace(value, valueNew);
                }

            return text2;
        }
        static public string checkSumSubstr(string text)
        {

            Stack<string> stack = new Stack<string>(text.Length);

            Regex multDiv2 = new Regex(@"\s*\d+\s*");
            Regex multDiv3 = new Regex(@"[\+\-]");

            MatchCollection ab = multDiv2.Matches(text);
            MatchCollection sumSub = multDiv3.Matches(text);

            if (ab.Count > 1)
            {
                stack.Push(Convert.ToString(ab[0]));

                int j = 0;
                for (int i = 1; i < ab.Count; i++, j++)
                {

                    stack.Push(Convert.ToString(ab[i]));
                    stack.Push(Convert.ToString(sumSub[j]));

                    string op = stack.Peek();
                    stack.Pop();

                    int y = Convert.ToInt32(stack.Peek());
                    stack.Pop();

                    int x = Convert.ToInt32(stack.Peek());
                    stack.Pop();

                    if (op == "+")
                        stack.Push(Convert.ToString(x + y));

                    if (op == "-")
                    {
                        if (text.IndexOf(Convert.ToString(x)) > 0 && text[text.IndexOf(Convert.ToString(x)) - 1] == '-')
                            if (text.IndexOf(Convert.ToString(y)) > 0 && text[text.IndexOf(Convert.ToString(y)) - 1] == '-')
                            {
                                stack.Push(Convert.ToString(-(x + y)));
                                continue;
                            }
                            else
                            {
                                stack.Push(Convert.ToString(y - x));
                                continue;
                            }
                        stack.Push(Convert.ToString(x - y));
                    }
                }

                return stack.Peek();
            }
            return text;
        }
        static public string swap(string item)
        {
            Regex reg = new Regex(@"\+\s?\-");

            item = reg.Replace(item, "-");

            Regex reg1 = new Regex(@"-\s?-");

            item = reg1.Replace(item, "+");

            return item;
        }
    }
    class Stack<T>
    {
        T[] data;
        public int Size { get; private set; }
        int flag = 0;

        public Stack(int length)
        {
            data = new T[length];
            Size = length;
        }

        public void Push(T value)
        {
            if (flag < Size)
            {
                data[flag] = value;
                flag++;
            }
        }
        public void Pop()
        {
            if (flag > 0)
            {
                Array.Clear(data, flag - 1, 1);
                flag--;
            }
        }
        public bool Contain()
        {
            if (flag == Size)
                return true;

            else
                return false;
        }
        public T Peek()
        {
            return data[flag - 1];
        }
    }
}
