﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stack
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите размер первого стэка типа int : ");
      
            int n = Convert.ToInt32(Console.ReadLine());
            Stack<int> stack1 = new Stack<int>(n);

            //Заполнение стэка
            Console.WriteLine("Введите данные....");

                while(!stack1.Contain())
                    stack1.Push(EnterNum());
            //Максимальный размер стэка
            Console.WriteLine("Максимальный размер : " + stack1.Size);
            

            //Выгрузка стэка
                for(int i = 0; i < n; i++)
                {
                    Console.Write(stack1.Peek() + " ");
                    stack1.Pop();
                }

            
            Console.Write("\nВведите размер второга стэка типа string : ");

            n = Convert.ToInt32(Console.ReadLine());
            Stack<string> stack2 = new Stack<string>(n);

            //Заполнение стэка
            Console.WriteLine("Введите данные....");

                while (!stack2.Contain())
                    stack2.Push(Console.ReadLine());

            //Максимальный размер стэка
            Console.WriteLine("Максимальный размер : " + stack2.Size);

            //Выгрузка стэка
                for (int i = 0; i < n; i++)
                {
                    Console.Write(stack2.Peek() + " ");
                    stack2.Pop();
                }   
            Console.ReadKey();
        }
        static public int EnterNum()
        {
            bool check;
            int num;

                do
                {
                    check = Int32.TryParse(Console.ReadLine(), out num);

                    if (!check)

                        Console.Write("........Введены неверные данные, попробуйте ещё раз........\n");

                } while (!check);

            return num;
        }
    }
    class Stack<T>
    {
        T[] data;
        public int Size { get; private set;}
        int flag = 0;

        public Stack(int length)
        {
            data = new T[length];
            Size = length;
        }

        public void Push(T value)
        {
            if (flag < Size)
            {
                data[flag] = value;
                flag++;
            }
        }
        public void Pop()
        {
            if (flag > 0)
            {
                Array.Clear(data, flag - 1, 1);
                flag--;
            }
        }
        public bool Contain()
        {
            if (flag  == Size)
                return true;

            else
                return false;
        }
        public T Peek()
        {
            return data[flag - 1];
        }
    }
}
