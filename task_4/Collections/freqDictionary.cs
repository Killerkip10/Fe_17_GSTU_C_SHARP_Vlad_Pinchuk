﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    class freqDictionary: Comparer<freqDictionary>
    {
        string word;
        int count;
         
        public override int Compare(freqDictionary a, freqDictionary b)
        {
            if (a.count > b.count)
                return -1;

            if (a.count < b.count)
                return 1;

            return 0;
        }
        public freqDictionary()
        {

        }
        public freqDictionary(string word, int count)
        {
            this.word = word;
            this.count = count;
        }
        public new string ToString()
        {
            return word + ' ' + count;
        }
    }
}
