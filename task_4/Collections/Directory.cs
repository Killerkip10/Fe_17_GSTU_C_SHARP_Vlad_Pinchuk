﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    class Directory
    {
        public string NameCompany { get; }
        public string Owner { get; }
        public string Phone { get; }
        public string Address { get; }
        public string Occupation { get; }

        public Directory(string name, string owner, string phone, string address, string occupation)
        {
            NameCompany = name;
            Owner = owner;
            Phone = phone;
            Address = address;
            Occupation = occupation;
        }
        public new string ToString()
        {
            return NameCompany + ' ' + Owner + ' ' + Phone + ' ' + Address + ' ' + Occupation;
        }
    }
}
