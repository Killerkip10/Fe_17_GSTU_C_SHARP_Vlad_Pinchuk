﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            menu();
        }
        static void menu()
        {
            int n = 0;

            do
            {
                Console.Clear();
                Console.WriteLine("1. Task_1");
                Console.WriteLine("2. Task_2");
                Console.WriteLine("0. Exit");
                n = EnterNum();

                Console.Clear();

                switch (n)
                {
                    case 1:
                        {
                            task_1();
                            Console.ReadKey();
                        }
                        break;

                    case 2:
                        {
                            task_2();
                        }
                        break;

                    case 0: break;
                    default: break;
                }

            } while (n != 0);
        }
        static public int EnterNum()
        {
            bool check;
            int num;

            do
            {
                check = Int32.TryParse(Console.ReadLine(), out num);

                if (!check)

                    Console.Write("......Entered incorrect data......\n......Repeat please......\n");

            } while (!check);

            return num;
        }
        static public string ReadFile(string file)
        {
            StreamReader reader = new StreamReader(file);
            string text = reader.ReadToEnd();
            reader.Close();

            return text;
        }

        static public List<Directory> CreateList()
        {
            var list = new List<Directory>();

            StreamReader reader = new StreamReader("directory.txt");

            string text = reader.ReadLine();

                do
                {
                    string[] textArr = text.Split('&');
                    list.Add(new Directory(textArr[0], textArr[1], textArr[2], textArr[3], textArr[4]));
                    text = reader.ReadLine();

                } while (text != null);

            reader.Close();

            return list;
        }
        static public void ShowAll(List<Directory> list)
        {
            foreach(var item in list)
                Console.WriteLine(item.ToString());
        }
        static public void SearchByName(List<Directory> list)
        {
            Console.Write("Enter name: ");
            string name = Console.ReadLine();

            var result = from value in list
                         where value.NameCompany == name
                         select value;

            foreach (var value in result)
                Console.WriteLine(value.ToString());
        }
        static public void SearchByOwner(List<Directory> list)
        {
            Console.Write("Enter Owner: ");
            string name = Console.ReadLine();

            var result = from value in list
                         where value.Owner == name
                         select value;

            foreach (var value in result)
                Console.WriteLine(value.ToString());
        }
        static public void SearchBySurname(List<Directory> list)
        {
            Console.WriteLine("-----Enter search interval-----");
            Console.Write("Enter first letter : ");
            string a = Console.ReadLine();

            Console.Write("Enter second letter : ");
            string b = Console.ReadLine();

                if(String.Compare(a, b) == 1)
                {
                    string c = a;
                    a = b;
                    b = c;
                }


            var result = list.Where(u => (String.Compare(u.Owner.ToLower(), a.ToLower()) == 1 || u.Owner.ToLower().StartsWith(a.ToLower()))
                                    && (String.Compare(u.Owner.ToLower(), b.ToLower()) == -1 || u.Owner.ToLower().StartsWith(b.ToLower())));
                        

            foreach (var value in result)
                Console.WriteLine(value.ToString());
        }
        static public void SearchByPhone(List<Directory> list)
        {
            Console.Write("Enter Phone: ");
            string name = Console.ReadLine();

            var result = from value in list
                         where value.Phone == name
                         select value;

            foreach (var value in result)
                Console.WriteLine(value.ToString());
        }
        static public void SearchByOccupation(List<Directory> list)
        {
            Console.Write("Enter Occupation: ");
            string name = Console.ReadLine();

            var result = from value in list
                         where value.Occupation == name
                         select value;

            foreach (var value in result)
                Console.WriteLine(value.ToString());
        }


        static public void task_1()
        {

            var list = new List<string>();
            var dictionary = new List<freqDictionary>();
            int position;
            int start = 0;
            string text = ReadFile("text.txt");

            Regex reg = new Regex(@"(\S+)[\s,\r\n]+");
            MatchCollection match = reg.Matches(text);

                for (int i = 0; i < match.Count; i++) 
                    list.Add(match[i].Groups[1].Value);

                while (list.Count > 0)
                {
                    string word = list[0];
                    int count = 0;
                    
                        for(int i = 0; i < list.Count; i++)
                        {
                            if(word == list[i])
                            {
                                list.RemoveAt(i);           
                                count++;
                                i--;
                            }
                                
                        }

                    dictionary.Add(new freqDictionary(word, count));
                }

            dictionary.Sort(new freqDictionary());

            foreach (freqDictionary item in dictionary)
                Console.WriteLine(item.ToString());
        }


        static public void task_2()
        {

            int n = 0;
            List<Directory> list = CreateList();

                do
                {
                    Console.Clear();
                    Console.WriteLine("1. Show all");
                    Console.WriteLine("2. Search by Name Company");
                    Console.WriteLine("3. Search by owner");
                    Console.WriteLine("4. Search by surname owner");
                    Console.WriteLine("5. Search by phone");
                    Console.WriteLine("6. Search by occupation");
                    Console.WriteLine("0. Back");

                    n = EnterNum();

                    Console.Clear();

                        switch (n)
                        {
                            case 1: {

                                ShowAll(list);
                                Console.ReadKey();
                            } break;

                            case 2: {

                                SearchByName(list);
                                Console.ReadKey();
                            } break;

                            case 3: {

                                SearchByOwner(list);
                                Console.ReadKey();
                            } break;

                            case 4: {

                                SearchBySurname(list);
                                Console.ReadKey();
                            } break;

                            case 5: {

                                SearchByPhone(list);
                                Console.ReadKey();
                            } break;

                            case 6: {

                                SearchByOccupation(list);
                                Console.ReadKey();
                            } break;

                            case 7: { } break;

                            case 0: { } break;

                            default: { }break;
                        }
                } while (n != 0);
        }
    }
}
